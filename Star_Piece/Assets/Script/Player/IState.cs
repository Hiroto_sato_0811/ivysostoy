﻿using UnityEngine;
using System.Collections;

public interface IState 
{
    void Initialize();  // 各状態での変数の初期化
    void Execution();   // 各状態の実行
    void NextState();   // 次のステートへ
}
