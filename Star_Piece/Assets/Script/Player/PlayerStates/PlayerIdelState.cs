﻿using UnityEngine;
using System.Collections;

public class PlayerIdelState : PlayerState
{
    public override string getStateName()
    {
        return "State:Idel";
    }

    public override void Execute()
    {
        Debug.Log("特別な処理がある場合は子が処理してもよい");

        if (execDelegate != null)
        {
            execDelegate();
        }
    }
}
