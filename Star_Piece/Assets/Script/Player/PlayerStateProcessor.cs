﻿using UnityEngine;
using System.Collections;


public class PlayerStateProcessor
{
    //ステート本体
    private PlayerState _State;

    public PlayerState State
    {
        set { _State = value; }
        get { return _State; }
    }

    // 実行ブリッジ
    public void Execute()
    {
        State.Execute();
    }

}