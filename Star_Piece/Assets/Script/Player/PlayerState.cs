﻿using UnityEngine;
using UnityEditor;

public　abstract class PlayerState
{
    //デリゲート
    public delegate void executeState();

    public executeState execDelegate;

    //実行処理
    public virtual void Execute()
    {
        if (execDelegate != null)
        {
            execDelegate();
        }
    }

    //ステート名を取得するメソッド
    public abstract string getStateName();
}