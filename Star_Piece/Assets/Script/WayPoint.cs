﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WayPoint : MonoBehaviour
{
    [SerializeField]
    private WayPoint //Northeast;
                     TopRight;
    [SerializeField]
    private WayPoint //East;
                     Right;
    [SerializeField]
    private WayPoint //Southeast;
                      BottomRight;
    [SerializeField]
    private WayPoint //Southwest;
                      BottomLeft;
    [SerializeField]
    private WayPoint //West;
                       Left;
    [SerializeField]
    private WayPoint //NorthWest;
                       TopLeft;

    public Vector3 pos;

    // Start is called before the first frame update
    void Start()
    {
      
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void RoadSelect(WayPoint point)
    {

    }
}
